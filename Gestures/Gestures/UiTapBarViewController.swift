//
//  UiTapBarViewController.swift
//  Gestures
//
//  Created by BRYAN OCAÑA on 23/1/18.
//  Copyright © 2018 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class UiTapBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        
        left.title = "Tap"
        right.title = "Swite"
        
        left.image = #imageLiteral(resourceName: "ic_fingerprint")
        right.image = #imageLiteral(resourceName: "ic_fingerprint")
    }
}

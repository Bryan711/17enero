//
//  SwiteViewController.swift
//  Gestures
//
//  Created by BRYAN OCAÑA on 17/1/18.
//  Copyright © 2018 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class SwiteViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func PinchExpand(_ gestureRecognizer: UIPinchGestureRecognizer) {
        
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            gestureRecognizer.view?.transform =
                (gestureRecognizer.view?.transform.scaledBy(
                    x: gestureRecognizer.scale, y: gestureRecognizer.scale))!;gestureRecognizer.scale = 1.0
        }
    }
    
    @IBAction func swipeUPAction(_ sender: Any) {
        
//        let action = sender as! UISwipeGestureRecognizer
//        velocity clacual la velocidad
        //tenemos la vista super rapido, si le hago rapoido q ocuep la pantalla
//        print(action,)
        customView.backgroundColor = .black
    }
    
    @IBAction func swipedownAction(_ sender: Any) {
        
        customView.backgroundColor = .blue
    }
    
    @IBAction func swipeleftAction(_ sender: Any) {
        
        customView.backgroundColor = .red
    }
    
    @IBAction func swiperightAction(_ sender: Any) {
        
        customView.backgroundColor = .green
    }
}

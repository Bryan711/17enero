//
//  TapGestureViewController.swift
//  Gestures
//
//  Created by BRYAN OCAÑA on 17/1/18.
//  Copyright © 2018 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {

    @IBOutlet weak var touchLabel: UILabel!
    @IBOutlet weak var tapLabel: UILabel!
    var taps = 0
    
    @IBOutlet weak var coordLabel: UILabel!
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touchcount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchLabel.text = "\(touchcount ?? 0)"
        tapLabel.text = "\(tapCount ?? 0)"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        coordLabel.text = "x: \(x ?? 0), y: \(y ?? 0)"
        //print("x: \(x), y: \(y)")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("termined!")
    }
    

    @IBAction func TapGestureAction(_ sender: UITapGestureRecognizer) {
        
        customView.backgroundColor = .red
//        if sender.state == .ended {
//            taps += 1
//            tapLabel.text = "\(taps)"
//        }
    }


}
